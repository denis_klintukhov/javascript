(function() {

	Hamburger.SIZE_SMALL = {name: 'Small', maxStuffings:5, callories:100, price: 1};
	Hamburger.SIZE_LARGE = {name: 'Large', maxStuffings:10, callories:200, price: 2};
	Hamburger.STUFFING_CHEESE = {stuffingName: 'cheese', callories:15, price: 0.5};
	Hamburger.STUFFING_SALAD = {stuffingName: 'salad', callories:5, price: 0.5}
	Hamburger.STUFFING_POTATO = {stuffingName: 'potato', callories:25, price: 0.7};
	Hamburger.TOPPING_MAYO = {stuffingName: 'mayo', callories:30, price: 0.5};
	Hamburger.TOPPING_SPICE = {stuffingName: 'spice', callories:5, price: 0.5};
	
		function Hamburger (size, stuffing) {
			this.stuffingsArray = [];
			this.toppingsArray = [];
			this.size = size;	
			this.stuffingsArray.push(stuffing);
			
			this.addStuffing = function (stuffing) {

			 // if(this.maxStuffings < this.stuffingsArray.length) {
			 // 	   this.stuffingsArray.push(stuffing);
				// 		this.all.push(stuffing);
				//    } 
					if(this.size === Hamburger.SIZE_SMALL) {
						
						if (this.stuffingsArray.length < Hamburger.SIZE_SMALL.maxStuffings) {
							this.stuffingsArray.push(stuffing);
						}

					}	if(this.size === Hamburger.SIZE_LARGE) {

							if (this.stuffingsArray.length < Hamburger.SIZE_LARGE.maxStuffings) {
								this.stuffingsArray.push(stuffing);
							}
					}
				};

				/**
				 * Добавить топпинг к гамбургеру. Можно добавить несколько,
				 * при условии, что они разные.
				 *
				 * @param topping  Тип топпинга
				 */
				this.addTopping = function (topping) {
					if(this.toppingsArray.indexOf(topping) == -1) {
						this.toppingsArray.push(topping);
					}
				};

				/**
				 * Убрать топпинг, при условии, что он ранее был
				 * добавлен.
				 *
				 * @param topping Тип топпинга
				 */
				this.removeTopping = function (topping) {
					let removeIndex = this.toppingsArray.indexOf(topping);
					if(removeIndex != -1) {
						this.toppingsArray.splice(removeIndex, 1);
					}
					
				};

				/**
				 * Узнать размер гамбургера
				 * @return {Number} размер гамбургера
				 */
				this.getSize = function () {
					return this.size.maxStuffings;
				};

				/**
				 * Узнать начинку гамбургера
				 * @return {Array} Массив добавленных начинок, содержит константы
				 * Hamburger.STUFFING_*
				 */
				this.getStuffing = function () {
					return this.stuffingsArray;
				};

				/**
				 * Получить список добавок
				 *
				 * @return {Array} Массив добавленных добавок, содержит константы
				 * Hamburger.TOPPING_*
				 */
				this.getToppings = function () {
					return this.toppingsArray;
				};

				/**
				 * Узнать калорийность
				 * @return {Number} Калорийность в калориях
				 */
				this.calculateCalories = function () {
					let calloriesStuffings = this.stuffingsArray.map((elem) => elem.callories).reduce((a, b) => a + b);
					let calloriesTopping = this.toppingsArray.map((elem) => elem.callories).reduce((a, b) => a + b);
					let callories = calloriesTopping  + calloriesStuffings + this.size.callories;
					return callories;
				};

				/**
				 * Узнать цену гамбургера
				 * @return {Number} Цена гамбургера
				 */
				this.calculatePrice = function () {
					let priceStuffings = this.stuffingsArray.map((elem) => elem.price).reduce((a, b) => a + b);
					let priceTopping = this.toppingsArray.map((elem) => elem.price).reduce((a, b) => a + b);
					let price = priceTopping  + priceStuffings + this.size.price;

					return price;
				};
	}

// маленький гамбургер с начинкой из сыра
var hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);

// // добавим из майонеза
hamburger.addTopping(Hamburger.TOPPING_MAYO);
console.log(hamburger)


// // добавим картофель
hamburger.addStuffing(Hamburger.STUFFING_POTATO);

// // спросим сколько там калорий
console.log('Калории: ' , hamburger.calculateCalories());

// // сколько стоит
console.log('Цена: ', hamburger.calculatePrice() + '$');

// // я тут передумал и решил добавить еще приправу
hamburger.addTopping(Hamburger.TOPPING_SPICE);

// // А сколько теперь стоит?
console.log('Цена с соусом ', hamburger.calculatePrice() + '$');

// // большой ли гамбургер получился?
console.log('Большой ли гамбургер? ', hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false

// // убрать добавку
hamburger.removeTopping(Hamburger.TOPPING_SPICE);

console.log('Сколько топпингов добавлено ', hamburger.getToppings().length); // 1

//--------------------------------


})();


